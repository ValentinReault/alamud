# -*- coding: utf-8 -*-
# Copyright (C) 2020 Valentin Reault et Quentin Mary, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import MangerEvent

class MangerAction(Action2):
    EVENT = MangerEvent
    RESOLVE_OBJECT = "resolve_for_use"
    ACTION = "eat"


