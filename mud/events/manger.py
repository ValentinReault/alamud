# -*- coding: utf-8 -*-
# Copyright (C) 2020 Valentin Reault et Quentin Mary, IUT d'Orléans
#==============================================================================

from .event import Event2

class MangerEvent(Event2):
    NAME = "eat"

    def perform(self):
        if not self.object.has_prop("eatable"):
            self.fail()
            return self.inform("eat.failed")
        self.object.move_to(self.actor.container())
        self.inform("eat")
